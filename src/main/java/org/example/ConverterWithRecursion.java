package org.example;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class ConverterWithRecursion {
    static String path = "src/main/java/org/example/";
    static String inputFileName = "udrumudru.xml";
    static String outputFileName = "udrumudru_updated.xml";
    static Map<String, String> changesMap = new HashMap<>();

    public static void main(String[] args) {

        try {
            Document xmlFileDocument = getXmlFileDocument(path + inputFileName);
            Node rootNode = xmlFileDocument.getDocumentElement();

            inSubtreeFindAndReplaceNodeTextContent(rootNode, "name", RandomPerson::getName);
            inSubtreeFindAndReplaceNodeTextContent(rootNode, "businessName", RandomCompany::getName);

            for (String changedTextContent : changesMap.keySet()) {
                System.out.println(changedTextContent + ":   " + changesMap.get(changedTextContent));
            }

            writeNewXmlFile(xmlFileDocument, path + outputFileName);
        } catch (ParserConfigurationException | IOException | SAXException | NoSuchMethodException |
                 InvocationTargetException | TransformerException | IllegalAccessException e) {
            System.out.println(e.getMessage());
        }

    }


    /**
     * @param sourceNode         The method performs a search in the subtree below the source node.
     * @param searchableNodeName The name of the node whose content will be modified.
     * @param getNewTextContent  Method to find new textual content
     */
    private static void inSubtreeFindAndReplaceNodeTextContent(
            Node sourceNode,
            String searchableNodeName,
            Supplier<String> getNewTextContent)
            throws InvocationTargetException, IllegalAccessException {

        // If the node has child nodes, the method recursively searches these nodes as well
        if (sourceNode.hasChildNodes()) {
            NodeList childNodes = sourceNode.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                if (childNodes.item(i).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                inSubtreeFindAndReplaceNodeTextContent(childNodes.item(i), searchableNodeName, getNewTextContent);
            }
        }

        // If name of sourceNode equals name of nodeToModify and the node has textual content, it will be modified
        if (sourceNode.getNodeName().equals(searchableNodeName)) {
            String nodeTextContent = sourceNode.getTextContent();
            if (nodeTextContent.length() > 0) {

                // dataMap contains (key, value) -> (old nodeTextContent, new nodeTextContent)
                // If the textual content of the node being viewed does not exist in the dataMap,
                // the new text content of the sourceNode will be generated using method getNewTextContent
                if (!changesMap.containsKey(nodeTextContent)) {
                    String newTextContent = getNewTextContent.get();
                    changesMap.put(nodeTextContent, newTextContent);
                }

                // The text content of the node will be replaced
                sourceNode.setTextContent(changesMap.get(nodeTextContent));
            }
        }
    }

    private static Document getXmlFileDocument(String filePath)
            throws ParserConfigurationException, IOException, SAXException, NoSuchMethodException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//        dbFactory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new File(filePath));
        doc.getDocumentElement().normalize();
        return doc;
    }

    private static void writeNewXmlFile(Document xmlFileDocument, String filePath) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(xmlFileDocument);
        StreamResult result = new StreamResult(new File(filePath));
        transformer.setOutputProperty(OutputKeys.INDENT, "no");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.transform(source, result);
        System.out.println("New xml file created: " + filePath);
    }

}