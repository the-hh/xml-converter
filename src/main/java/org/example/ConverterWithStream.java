package org.example;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.IntStream;

public class ConverterWithStream {
    static String path = "src/main/java/com/hh/mockdata/xml/";
    static String inputFileName = "udrumudru.xml";
    static String outputFileName = "udrumudru_updated.xml";
    static Map<String, String> changesMap = new HashMap<>();

    public static void main(String[] args) {

        try {
            Document xmlFileDocument = getXmlFileDocument(path + inputFileName);

            findAndReplaceNodeTextContent(xmlFileDocument, "name", RandomPerson::getName);
            findAndReplaceNodeTextContent(xmlFileDocument, "businessName", RandomCompany::getName);

            for (String changedTextContent : changesMap.keySet()) {
                System.out.println(changedTextContent + ":   " + changesMap.get(changedTextContent));
            }

            writeNewXmlFile(xmlFileDocument, path + outputFileName);

        } catch (ParserConfigurationException | IOException | SAXException | NoSuchMethodException |
                 TransformerException e) {
            System.out.println(e.getMessage());
        }

    }

    private static void findAndReplaceNodeTextContent(Document xmlFileDocument, String nodeName, Supplier<String> getNewTextContent) {
        NodeList nList = xmlFileDocument.getElementsByTagName(nodeName);
        IntStream.range(0, nList.getLength())
                .mapToObj(nList::item)
                .filter(n -> n.getNodeType() == Node.ELEMENT_NODE)
                .filter(n -> n.getTextContent().length() > 0)
                .forEach(n -> textContentConversion(n, getNewTextContent));
    }

    private static void textContentConversion(Node sourceNode, Supplier<String> getNewTextContent) {
        // dataMap contains (key, value) -> (old nodeTextContent, new nodeTextContent)
        // If the textual content of the node being viewed does not exist in the dataMap,
        // the new text content of the sourceNode will be generated using method getNewTextContent
        String textContent = sourceNode.getTextContent();
        if (!changesMap.containsKey(textContent)) {
            String newTextContent = getNewTextContent.get();
            changesMap.put(textContent, newTextContent);
        }
        // The text content of the node will be replaced
        sourceNode.setTextContent(changesMap.get(textContent));
    }

    private static Document getXmlFileDocument(String filePath)
            throws ParserConfigurationException, IOException, SAXException, NoSuchMethodException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//        dbFactory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new File(filePath));
        doc.getDocumentElement().normalize();
        return doc;
    }

    private static void writeNewXmlFile(Document xmlFileDocument, String filePath) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(xmlFileDocument);
        StreamResult result = new StreamResult(new File(filePath));
//        transformer.setOutputProperty(OutputKeys.INDENT, "no");
//        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.transform(source, result);
        System.out.println("New xml file created: " + filePath);
    }

}