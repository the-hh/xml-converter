package org.example;

import java.util.Random;

public class RandomPerson {

    static Random rand = new Random();

    public static String getName() {
        return getFirstname() + " " + getLastname();
    }

    public static String getFirstname() {
        String[] firstnames = new String[] { "ALEKSANDR", "VLADIMIR", "SERGEI", "ANDREI", "ALEKSEI", "ANDRES", "MARTIN",
                "IGOR", "DMITRI", "VIKTOR", "TOOMAS", "MARGUS", "REIN", "OLEG", "JAAN", "KRISTJAN", "URMAS", "NIKOLAI",
                "ALEXANDER", "AIVAR", "ROMAN", "PEETER", "MAKSIM", "MARKO", "JEVGENI", "TARMO", "JURI", "PRIIT",
                "INDREK", "MATI", "ALEKSANDER", "ARTUR", "MEELIS", "VALERI", "MIHHAIL", "RAIVO", "IVAN", "ANDRUS",
                "SANDER", "JAANUS", "PAVEL", "TIIT", "SERGEY", "MAREK", "MIHKEL", "SIIM", "ANTS", "VELLO", "SVETLANA",
                "VALENTINA", "GALINA", "ANNA", "MARIA", "NATALJA", "MARINA", "TIINA", "KATRIN", "SIRJE" };
        int index = rand.nextInt(firstnames.length);
        return firstnames[index];
    }

    public static String getLastname() {
        String[] lastnames = new String[] { "AAS", "AASA", "AAVIK", "ADAMSON", "ALEKSANDROV", "ALEKSEEV", "ALLIK",
                "ANDERSON", "ANDREEV", "ANTON", "ANTONOV", "ARRO", "ARU", "AUS", "BARANOV", "BELOV", "BOGDANOV",
                "DANILOV", "DMITRIJEV", "FILIPPOV", "FJODOROV", "FROLOV", "GAVRILOV", "GOLUBEV", "GRIGORJEV", "GUSEV",
                "HALLIK", "HANSEN", "HEIN", "HUNT", "ILJIN", "ILVES", "IVANOV", "JAKOBSON", "JAKOVLEV", "JEFIMOV",
                "JEGOROV", "JOHANSON", "KAASIK", "KADAK", "KALA", "KALDA", "KALININ", "KALLAS", "KALLASTE", "KANGRO",
                "KANGUR", "KARPOV", "KARRO", "KARU", "KASE", "KASK", "KAUR", "KIKAS", "KIRS", "KIVI", "KIVISTIK",
                "KOKK", "KOLK", "KOORT", "KOPPEL", "KOZLOV", "KOTKAS", "KRUUS", "KUKK", "KULL", "KURG", "KURVITS",
                "KUZMIN", "KUZNETSOV", "KUUS", "KUUSIK", "KUUSK", "LAANEMETS", "LAAS", "LAUR", "LEBEDEV", "LEMBER",
                "LEPIK", "LEPP", "LEPPIK", "LIIV", "LIIVA", "LILL", "LIND", "LUHT", "LUIK", "LUTS", "MAASIK", "MAKAROV",
                "MAKSIMOV", "MARIPUU", "MARTIN", "MATVEJEV", "MEIER", "MELNIKOV", "METS", "MIHHAILOV", "MIKK", "MILLER",
                "MITT", "MOROZOV", "MUST", "NIKIFOROV", "NIKITIN", "NIKOLAEV", "NOVIKOV", "NURK", "OJA", "ORAV",
                "ORLOV", "OTS", "PAAS", "PAJU", "PALM", "PALU", "PARTS", "PAVLOV", "PETERSON", "PETROV", "PIHLAK",
                "PIIR", "PLOOM", "POLJAKOV", "POPOV", "POST", "PUKK", "PUUSEPP", "RAND", "RAUD", "RAUD-SEPP", "REBANE",
                "ROHTLA", "ROMANOV", "ROOS", "ROOTS", "ROSENBERG", "SAAR", "SAKS", "SARAPUU", "SARV", "SEMJONOV",
                "SEPP", "SERGEJEV", "SIKK", "SILD", "SMIRNOV", "SOKOLOV", "SOLOVJOV", "SOOSAAR", "SOROKIN", "STEPANOV",
                "SUSI", "ZAHHAROV", "ZAITSEV", "TALI", "TALTS", "TAMM", "TAMME", "TAMMIK", "TEDER", "TILK", "TIMOFEJEV",
                "TOMINGAS", "TOMSON", "TOOM", "TOOME", "TOOMING", "TOOMSALU", "TOOTS", "TROFIMOV", "TSVETKOV", "UIBO",
                "UNT", "UUSTALU", "VAHER", "VAHTER", "VAHTRA", "VAINO", "VALGE", "VALK", "VARES", "VARIK", "VASILIEV",
                "VASSILJEV", "VESKI", "VIIRA", "VINOGRADOV", "VOLKOV", "VOROBJOV" };
        int index = rand.nextInt(lastnames.length);
        return lastnames[index];
    }
}
